FROM nginx:latest

RUN apt-get update && apt-get -y upgrade

COPY docker/nginx.conf   /etc/nginx/nginx.conf
COPY docker/default.conf /etc/nginx/conf.d/default.conf

COPY ./dist/       /var/www

CMD ["/usr/sbin/nginx"]
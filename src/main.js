import Vue from "vue";
import App from "./App.vue";
import {
  digitalFlop,
  borderBox12,
  conicalColumnChart,
  scrollBoard,
  capsuleChart,
} from "@jiaminghi/data-view";

import "swiper/swiper-bundle.css";
import "./assets/css/font.css";

Vue.use(digitalFlop);
Vue.use(borderBox12);
Vue.use(scrollBoard);
Vue.use(conicalColumnChart);
Vue.use(capsuleChart);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
